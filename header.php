<!DOCTYPE html>
<html>

<head>
	
	<meta charset="UTF-8">
	<title>GOOD corps</title>
	<link rel="stylesheet" href="_css/base.css">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	
	<script src="_js/jquery.js"></script>
	<script src="_js/jquery.jscroll.js"></script>
	<script src="_js/main.js"></script>
	<script src="_js/typed.js"></script>
	
 


	<!-- SOCIAL META -->
	
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:site" content="@goodcorps"/>
	<meta name="twitter:title" content="GOOD corps"/>
	<meta name="twitter:description" content="Vestibulum laoreet ut nulla sed convallis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. "/>
	<meta name="twitter:image:src" content=""/>
	
	<meta property="og:title" content="GOOD corps"/>
	<meta property="og:site_name" content="We partner with brands, foundations, & nonprofits to demonstrate authentic leadership"/>
	<meta property="og:description" content="Vestibulum laoreet ut nulla sed convallis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. "/>
	<meta property="og:type" content="website"/>
	<meta property="og:image" content=""/>
	<meta property="og:locale" content="en_us"/>


  
</head>

<body>

	<div class="goodnav"></div>

	<div class="ruler"></div>
	<div class="ruler two"></div>

	<header>
		<div class="name">
			
			<a href="index.php" class="logo"><img src="_img/logo.svg"></a>
							
		</div>
		
		<nav class="mobile">
		
			<ul>
				<li>
					<a href="">About</a>
				</li>

				<li>
					<a href="">People</a>
				</li>

				<li>
					<a href="work.php">Work</a>
				</li>

				<li>
					<a href="news.php">News</a>
				</li>
			</ul>
		
		</nav>

		<nav class="desktop">
			<ul>
				<li>
					<div class="line"><div></div></div>
				</li>
				
				<!--
<li>
					<a href="" id="about">Process</a>
				</li>

				<li>
					<a href="" id="people">People</a>
				</li>
-->

				<li>
					<a href="work.php" id="work">Projects</a>
				</li>

				<li>
					<a href="news.php" id="news">News</a>
				</li>

				<li>
					<a href="#" class="scroll" id="Contact_Arealink" >Contact</a>
				</li>
			</ul>
		</nav>
	
	</header>
