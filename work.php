<?php include 'header.php';?>

<div class="content work">

	<section class="intro">
		<div class="row">
		    <div class="col-md-offset-3 col-md-9 col-xs-12">
				<h1>Projects</h1>
		    </div>
		    <div class="col-md-offset-3 col-md-9 col-xs-12">
		    	<div class="work_filter">
		    	
		    	<ul>
		    		<li><a class="current" href="">All</a></li>
		    		<li><a href="">Strategy</a></li>
		    		<li><a href="">Marketing</a></li>
		    		<li><a href="">Programs</a></li>
		    		<li><a href="">Installations</a></li>
		    		<li><a href="">Content</a></li>
		    		<li><a href="">Products</a></li>
		    	</ul>
		    	
		    	</div>
			</div>
		</div>
		
	</section>
</div>



<div class="work_cont">

	
	<div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_1.jpg">
	
	  </div> 
	
	
	
	<div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_2.jpg">
	
	 
	</div>
	  
	  <div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_3.jpg">
	
	 
	</div>
	  
	  <div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_4.jpg">
	
	 
	</div>
  
 
  
</div>
  
  
  
  
<?php include 'footer.php';?>



