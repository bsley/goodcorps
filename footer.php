<script src="_js/jquery.js"></script>


<footer id="Contact_Area">

<div class="row">

	<div class="col-xs-12 col-md-offset-0 col-md-4">
		<div class="line"><div></div></div>
		
		<h1>Contact</h1>
	</div>



	<div class="col-xs-12 col-md-offset-0 col-md-4">
		<p>
		<a href="">hello@goodcorps.com</a><br>
		—<br>
		4492 Michael Street <br>
		Los Angeles, CA 77063<br>
		310-432-3422<br>
		
		</p>
	</div>
	
	
	
	<div class="col-xs-12 col-md-offset-0 col-md-2">
	
		<div class="social">
			<a class="facebook" href="https://www.facebook.com/goodcorps" target="_blank">Facebook</a> 			
			<a class="twitter" href="https://twitter.com/goodcorps" target="_blank">Twitter</a> 			
			<a class="linkedin" href="https://www.linkedin.com/company/good-corps" target="_blank">Linkedin</a>
		</div>
	
	
	</div>
	
	
	
	
	
</div>


<div class="row">

<div class="col-xs-12 col-md-offset-0 col-md-4">
		<p class="copyright">
		© 2014 GOOD Worldwide, Inc.<br>

		
		</p>
	
	</div>
	<div class="col-xs-12 col-md-offset-0 col-md-4">
		<p>
<!-- 			<a href="http://magazine.good.is/">good.is</a><br> -->
		</p>
	</div>
	<div class="col-xs-12 col-md-offset-0 col-md-2">
		<p>
<!-- 			<a href="http://good.theresumator.com/">Careers</a> -->
		</p>
	</div>


</div>




</footer>


</body>

</html>

