
<?php include 'header.php';?>

<script>

$(document).ready(function()  {
	$('header #news').addClass("current");
});

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=254167767997169&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



	
<div class="content news detail">


	<div class="row intro entry">
	    <div class="col-xs-offset-0 col-xs-12 col-md-offset-3 col-md-9">
	    
	    	<p class="back_button"><a href="news.php">&lsaquo; &nbsp; Back to News</a></p>
			<h1>GOOD corps for Mastercard + Stand Up For Cancer</h1>
<!-- 			<p></p> -->
			
			<p>
			November 1, 2014 &mdash; Chances are you've wandered through your city's street markets before. Perhaps you went to purchase some locally-grown produce or handcrafted goods, but maybe hadn't thought about these bustling centers as a point of origin for much of your city's culture. The items and foods that these local vendors sell actually tell a great deal about your home, and often these marketplaces were the jumping off points to larger business hubs. Take a closer look at some famous street markets around the world, and learn which wares and goods they're best known for and have helped shape the places and people around them.
			</p>
			
			<img src="http://media.virbcdn.com/cdn_images/resize_1600x1600/ae/PageImage-531144-5235322-finalselectsjared11.jpg">
			
			<p>
			And so we here at Walgreens, together with our partners at Vitamin Angels and GOOD, are excited to launch the GOOD Wellness Project. Through an editorial series, slideshows, a documentary video, and an interactive infographic, we will explore how to make healthier and smarter decisions, especially in the case of our personal nutrition and the role vitamins can play in that.

Why vitamins, you might ask? Vitamins are especially personal to Walgreens—they’re nutrients that are essential for healthy growth, vitality, and general well-being. We know that, for many, basic nutrition is something most don’t even need to think twice about. However, there are many people around the world who suffer from life-threatening illnesses simply because they don’t get enough essential nutrients in their daily diets. Since last year, Walgreens has partnered with Vitamin Angels, helping at-risk populations in need—specifically pregnant women, new mothers, and children under five—gain access to lifesaving and life changing vitamins and minerals.			</p>
						
			<ul>
				<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
				<li>Vivamus varius eros lacinia ipsum varius lobortis.</li>
				<li>Donec lacinia justo sit amet elit egestas convallis.</li>
				<li>Aenean finibus nisl in tellus maximus convallis.</li>
			</ul>
			
			<p>
			Nam a tortor massa. Aenean id fermentum mi. Ut ac tellus nec quam pellentesque consequat vel eu neque. Pellentesque fermentum risus vitae urna imperdiet faucibus. Duis ex sapien, vehicula id dolor quis, commodo lobortis nisl. Suspendisse potenti.
			</p>
			
			<ol>
				<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
				<li>Vivamus varius eros lacinia ipsum varius lobortis.</li>
				<li>Donec lacinia justo sit amet elit egestas convallis.</li>
				<li>Aenean finibus nisl in tellus maximus convallis.</li>
			</ol>
			
			<p>
			Phasellus vehicula lacus vitae velit pulvinar, accumsan pharetra turpis porta. Cras id nunc ex. Donec suscipit volutpat orci, eget rhoncus diam lacinia ac. Donec efficitur metus ut turpis tristique, sit amet efficitur tortor laoreet. Maecenas eu orci lectus. Nam non luctus libero, eu lobortis quam. Nullam venenatis nisl lorem, eget fermentum lorem luctus nec. Phasellus blandit erat et ex mattis, eu euismod ante lacinia. Duis interdum ac libero sit amet sollicitudin.
			</p>
			
			<blockquote>
				People demand freedom of speech as a compensation for the freedom of thought which they seldom use. - Soren Kierkegaard
			</blockquote>
			
			<p>
			Morbi accumsan est eu lectus pretium, ut vestibulum risus dapibus. Etiam sapien lorem, elementum ac venenatis eget, semper ut ex. Cras ac lectus neque. Integer malesuada ipsum eros, eget fermentum eros tempus quis. In hac habitasse platea dictumst. Praesent pretium, magna sed eleifend cursus, purus dolor accumsan mi, sed ullamcorper neque neque et mi. In laoreet, nisl non interdum bibendum, lacus ex sollicitudin lorem, sed tempor mauris odio sed metus. Etiam quis venenatis ligula. Phasellus vel dapibus ipsum. Nulla ut pharetra nisl, id malesuada ex. Suspendisse vitae faucibus turpis. Aliquam ut suscipit nisl. Suspendisse tincidunt est vel neque pellentesque accumsan vitae vitae eros. Etiam non ex nisl. Etiam eros mauris, consequat at erat ultrices, sollicitudin tempus turpis. Pellentesque malesuada orci ac dolor consectetur consequat.
			</p>
			
			<br><br>
			<p>&mdash;</p>
			<br><br>

			<div class="sharing">
			
<!-- 			<h3>Share This:</h3> -->
			
				<div class="fb-share-button" data-href="{site_url}blog/post/{url_title}" data-layout="button_count"></div>
		
			
				<div class="tweet">
					<a 
					
					href="https://twitter.com/share" 
					class="twitter-share-button" 
					data-via="iwantrest" 
					data-lang="en"
					data-text="{title}"
					>Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
		
			</div>
	
	</div>

</div>



	

<?php include 'footer.php';?>
