<?php include 'header.php';?>


	
<div class="content home">

	<section class="intro">
		<div class="row">
		    <div class="col-md-offset-3 col-md-9 col-xs-12">
				<!-- <h1>We partner with brands, foundations, & nonprofits to demonstrate authentic leadership.</h1> -->
				<h1>We're a social innovation consultancy <a href="http://magazine.good.is/" target="_blank">@GOOD</a> headquarters, partnering with great intrapreneurs to help them accomplish the maximum social benefit.</h1>
				<p class="headline"></p>
		    </div>
		</div>
		
	</section>
	
	<div class="illo home"><img src="_img/arrows.svg"></div>


	<section class="process">
		<div class="row">
		
		    <div class="col-md-offset-7 col-md-5 col-xs-12">
		    	<h2>Our agile problem solving approach brings together best practices from the world of social impact, design & innovation and management consulting.</h2>
				<p><a href="" class="underlined negative">See how we do things.</a></p>
	
			</div>
		
		</div>
	</section>
	
	
<!-- 	WORK SECTION -->

<div class="work_header">

	<div class="row">

		<div class="col-xs-12 col-md-offset-0 col-md-6">
	    	<div class="area_title">
	    		<div class="line"><div></div></div>
	    		<h1>Projects</h1>
	    		<p class="seemore"><a href="work.php" class="underlined">See all projects</a></p>
	    	</div>
	    </div>
	    
	 </div>

</div>


<div class="work_cont">

	
	<div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_1.jpg">
	
	  </div> 
	
	
	
	<div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_2.jpg">
	
	 
	</div>
	  
	  <div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_3.jpg">
	
	 
	</div>
	  
	  <div class="work_thumb linkeddiv">
	    <section class="inside">
	      <p class="title"><a href="work_detail.php">Title of the project</a></p>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue ipsum in commodo mattis. Nunc ac placerat odio. </p>
	      <p><a href="" class="underlined negative">See Project</a></p>
	    </section>
	    
	    <img src="_img/thumb_4.jpg">
	
	 
	</div>
  
 
  
</div>
	
	
	<div class="row news">
	
	    <div class="col-xs-12 col-md-offset-0 col-md-6">
	    	<div class="area_title">
	    		<div class="line"><div></div></div>
	    		<h1>News</h1>
	    	</div>
	    </div>
	    
	    <div class="col-xs-12 col-md-offset-0  col-md-12 news_preview">
	    
	    <div class="row">

	    	<div class="col-md-4 col-xs-12 entry linkeddiv">
		    	<h3><a href="news_detail.php">GOOD corps for Mastercard + Stand Up</a></h3>
		    	<p class="desc">November 23, 2014  —  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut consequat enim ipsum, at auctor ante pharetra sit amet. Nam id mi aliquam magna vestibulum... </p>
<!-- 				<p><a href="news_detail.php" class="underlined">Read More</a></p> -->
			</div>
			
			<div class="col-md-4 col-xs-12 entry linkeddiv">
		    	<h3><a href="news_detail.php">Proin maximus eros nisi, a tristique lands</a></h3>
		    	<p class="desc">November 23, 2014  —  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut consequat enim ipsum, at auctor ante pharetra sit amet. Nam id mi aliquam magna vfacilisis... </p>
<!-- 				<p><a href="news_detail.php" class="underlined">Read More</a></p> -->
			</div>
			
			<div class="col-md-4 col-xs-12 entry linkeddiv">
		    	<h3><a href="news_detail.php">Proin maximus eros nisi, a tristique leo sodales at</a></h3>
		    	<p class="desc">November 23, 2014  —  We work with our clients to identify openings in the landscape aligned to competitive advantage and social impact, to develop actionable...</p>
<!-- 				<p><a href="news_detail.php" class="underlined">Read More</a></p> -->
			</div>
			
			</div>

		</div>
		
		<div class="col-xs-12 col-md-offset-0 col-md-6 seemore">
			<p><a href="news.php" class="underlined">See more news</a></p>
	    </div>
		
	
	</div>
	
	<!--
<div class="contact_area" id="Contact_Area">
	
		<div class="row">
			
			<div class="col-xs-12">
				info@goodcorps.com
			</div>
		
		</div>
	
	</div>
-->
	

</div>



	

<?php include 'footer.php';?>
