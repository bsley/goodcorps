
<?php include 'header.php';?>

<script>

$(document).ready(function()  {
	$('header #work').addClass("current");
});

</script>

	
<div class="content work detail">


	<div class="row intro entry">
	    <div class="col-xs-offset-0 col-xs-12 col-md-offset-3 col-md-9">
	    
	    	<p class="back_button"><a href="work.php">&lsaquo; &nbsp; Back to Projects</a></p>
			<h1>Title of the project</h1>

			<p>Since last year, Walgreens has partnered with Vitamin Angels, helping at-risk populations in need—specifically pregnant women, new mothers, and children under five.</p>

			<img class="feat_image" src="http://media.virbcdn.com/cdn_images/resize_1600x1600/ae/PageImage-531144-5235322-finalselectsjared11.jpg">

			<p>And so we here at Walgreens, together with our partners at Vitamin Angels and GOOD, are excited to launch the GOOD Wellness Project. Through an editorial series, slideshows, a documentary video, and an interactive infographic, we will explore how to make healthier and smarter decisions, especially in the case of our personal nutrition and the role vitamins can play in that. Why vitamins, you might ask? Vitamins are especially personal to Walgreens—they’re nutrients that are essential for healthy growth, vitality, and general well-being. We know that, for many, basic nutrition is something most don’t even need to think twice about. However, there are many people around the world who suffer from life-threatening illnesses simply because they don’t get enough essential nutrients in their daily diets. Since last year, Walgreens has partnered with Vitamin Angels, helping at-risk populations in need—specifically pregnant women, new mothers, and children under five—gain access to lifesaving and life changing vitamins and minerals.</p>

			<p>—</p>

			<p>Client: Walgreens<br>
			   Date: Launched August 2013<br>
			   Project Lead: Maria Redin
			</p>


			
			
		</div>
	
	</div>

</div>



	

<?php include 'footer.php';?>
